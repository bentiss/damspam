# SPDX-Licences-Identifier: MIT
#
# This file is formatted with Python Black

from click.testing import CliRunner
from unittest.mock import patch, MagicMock, NonCallableMagicMock
from pathlib import Path

import damspam

import attr
import datetime
import logging
import pytest

logger = logging.getLogger("test-cli")


# Patch the cli to use example.com as default instance (which is also used
# in the example webhook payloads). This way a runway test
# doesn't risk contacting the real fdo gitlab
@pytest.fixture(autouse=True)
def replace_default_instance():
    with patch("damspam.cli.DEFAULT_INSTANCE", "https://example.com/"):
        yield


def write_xdg_token(name: str = "spambot.token", project="damspam"):
    token_path = Path(project) / name
    token_path.parent.mkdir(parents=True)
    with open(token_path, "w") as f:
        f.write(f"xdgtokenvalue.{name}")


@pytest.mark.parametrize(
    "command",
    (["hide-issue", "ns/subns/project#123"],),
)
@patch("damspam.cli.ds")
def test_damspam_token_lookup(ds, monkeypatch, command):
    from damspam.cli import damspam as damspam_cli

    logger.debug(f"Arguments are: {command}")
    expected_url = "https://example.com/"

    # Disable the XDG token lookup
    monkeypatch.setattr(Path, "home", lambda: Path("/does-not-exist"))
    monkeypatch.setenv("OUR_TOKEN", "envtokenvalue")

    builder = MagicMock()
    builder.create_from_url = MagicMock(return_value=builder)
    builder.set_project = MagicMock(return_value=builder)
    builder.set_issue_iid = MagicMock(return_value=builder)
    builder.set_readonly = MagicMock(return_value=builder)

    # Override the entry point with our mock
    ds.Builder = builder

    # Not techically needed since the CLI gets the object from the builder
    # and defaults to a MagicMock but this makes sure we blow up if we
    # somehow get around it
    ds.Issue = NonCallableMagicMock()
    ds.SPAM_LABEL = damspam.SPAM_LABEL

    # No token, so it fails
    runner = CliRunner()
    with runner.isolated_filesystem():
        result = runner.invoke(damspam_cli, command)
        logger.debug(result.output)
        assert result.exit_code == 1
        assert "One of --token-file or --token-env is required" in result.output

    monkeypatch.setenv("XDG_CONFIG_HOME", ".")

    # Token file should work
    with runner.isolated_filesystem():
        # xdg token exists but is ignored
        write_xdg_token()

        # token we want to use
        with open("token.txt", "w") as f:
            f.write("tokenvalue")

        result = runner.invoke(damspam_cli, ["--token-file", "token.txt"] + command)
        logger.debug(result)
        logger.debug(f"Output: {result.output}")
        assert result.exit_code == 0

        builder.create_from_url.assert_called_with(
            expected_url,
            "tokenvalue",
        )

    # Token env should work
    with runner.isolated_filesystem():
        # xdg token exists but is ignored
        write_xdg_token()

        result = runner.invoke(damspam_cli, ["--token-env", "OUR_TOKEN"] + command)
        logger.debug(result.output)
        assert result.exit_code == 0

        ds.Builder.create_from_url.assert_called_with(
            expected_url,
            "envtokenvalue",
        )

    # XDG token should work
    with runner.isolated_filesystem():
        monkeypatch.setenv("XDG_CONFIG_HOME", ".")
        write_xdg_token()

        result = runner.invoke(damspam_cli, command)
        logger.debug(result.output)
        assert result.exit_code == 0

        ds.Builder.create_from_url.assert_called_with(
            expected_url,
            "xdgtokenvalue.spambot.token",
        )


@pytest.mark.parametrize(
    "issue_arg",
    [
        "ns/subns/project-name#123",
        "https://example.com/ns/subns/project-name/-/issues/123",
    ],
)
@patch("damspam.cli.ds")
def test_damspam_hide_issue_parse_issue_arg(ds, monkeypatch, issue_arg):
    from damspam.cli import damspam as damspam_cli

    monkeypatch.setenv("XDG_CONFIG_HOME", ".")

    builder = MagicMock()
    builder.create_from_url = MagicMock(return_value=builder)
    builder.set_project = MagicMock(return_value=builder)
    builder.set_issue_iid = MagicMock(return_value=builder)
    builder.set_readonly = MagicMock(return_value=builder)

    # Override the entry point with our mock
    ds.Builder = builder

    # Not techically needed since the CLI gets the object from the builder
    # and defaults to a MagicMock but this makes sure we blow up if we
    # somehow get around it
    ds.Issue = NonCallableMagicMock()

    runner = CliRunner()
    with runner.isolated_filesystem():
        write_xdg_token()
        result = runner.invoke(damspam_cli, ["hide-issue", issue_arg])
        logger.debug(result.output)
        assert result.exit_code == 0

        ds.Builder.set_project.assert_called_with("ns/subns/project-name")
        ds.Builder.set_issue_iid.assert_called_with(123)

    with runner.isolated_filesystem():
        write_xdg_token()
        result = runner.invoke(
            damspam_cli,
            ["--instance=https://gitlab.somewhere.com", "hide-issue", issue_arg],
        )
        logger.debug(result.output)

        # we only care about an instance mismatch if we have a full URL
        if issue_arg.startswith("https:"):
            assert result.exit_code == 1
            assert "Invalid URL, --instance arg required" in result.output
        else:
            assert result.exit_code == 0

        builder.set_project.assert_called_with("ns/subns/project-name")
        builder.set_issue_iid.assert_called_with(123)


@pytest.mark.parametrize(
    "project", ("ns/subns/project", "https://example.com/ns/subns/project")
)
@pytest.mark.parametrize("is_maintainer", (True, False))
@patch("damspam.cli.ds")
def test_request_webhook(ds, monkeypatch, project: str, is_maintainer: bool):
    from damspam.cli import damspam as damspam_cli

    monkeypatch.setenv("XDG_CONFIG_HOME", ".")

    builder = MagicMock()
    builder.create_from_url = MagicMock(return_value=builder)
    builder.set_project = MagicMock(return_value=builder)
    builder.set_readonly = MagicMock(return_value=builder)
    builder.build_project = MagicMock(return_value=builder)

    builder.file_webhook_request_issue = MagicMock(return_value=builder)
    builder.user_is_maintainer = MagicMock(return_value=is_maintainer)

    # Override the entry point with our mock
    ds.Builder = builder

    # Not techically needed since the CLI gets the object from the builder
    # and defaults to a MagicMock but this makes sure we blow up if we
    # somehow get around it
    ds.Project = NonCallableMagicMock()

    runner = CliRunner()
    with runner.isolated_filesystem():
        write_xdg_token("user.token")

        result = runner.invoke(damspam_cli, ["request-webhook", project])
        logger.debug(result)
        logger.debug(f"Output: {result.output}")
        if is_maintainer:
            assert not result.exception
            assert result.exit_code == 0
        else:
            assert isinstance(result.exception, SystemExit)
            assert result.exit_code == 1

        builder.user_is_maintainer.assert_called()
        if is_maintainer:
            builder.file_webhook_request_issue.assert_called()
        else:
            builder.file_webhook_request_issue.assert_not_called()


@patch("damspam.cli.ds")
def test_sanitize_webhook(ds, monkeypatch):
    from damspam.cli import damspam as damspam_cli

    monkeypatch.setenv("XDG_CONFIG_HOME", ".")

    builder = MagicMock()
    builder.create_from_url = MagicMock(return_value=builder)
    builder.set_username = MagicMock(return_value=builder)
    builder.set_readonly = MagicMock(return_value=builder)

    # Override the entry point with our mock
    ds.Builder = builder

    # Not techically needed since the CLI gets the object from the builder
    # and defaults to a MagicMock but this makes sure we blow up if we
    # somehow get around it
    ds.Issue = NonCallableMagicMock()

    runner = CliRunner()
    with runner.isolated_filesystem():
        write_xdg_token()

        result = runner.invoke(
            damspam_cli,
            ["sanitize-profile", "--delay=0", "--use-user-id", "41"],
        )
        logger.debug(result)
        logger.debug(f"Output: {result.output}")
        assert not result.exception
        assert result.exit_code == 0

        builder.create_from_url.assert_called_with(
            "https://example.com/",
            "xdgtokenvalue.spambot.token",
        )
        builder.set_user_id.assert_called_with(
            41,
        )
        builder.set_username.assert_not_called()


@patch("damspam.cli.ds")
@pytest.mark.parametrize("since", ("2h ago", "24h ago"))
def test_sanitize_profiles(ds, since, monkeypatch):
    from damspam.cli import damspam as damspam_cli

    monkeypatch.setenv("XDG_CONFIG_HOME", ".")

    users = [MagicMock(), MagicMock(), MagicMock()]

    def list_spammers(cutoff, queue, include_sanitized_users):
        if since == "2h ago":
            expected_cutoff = datetime.timedelta(hours=2)
        elif since == "24h ago":
            expected_cutoff = datetime.timedelta(hours=24)
        else:
            assert False, "Invalid value for 'since'"

        error_margin = datetime.timedelta(seconds=10)
        now = datetime.datetime.now(tz=datetime.timezone.utc)
        assert cutoff > now - expected_cutoff - error_margin
        assert cutoff < now - expected_cutoff + error_margin

        assert include_sanitized_users is False

        for user in users:
            queue.put(user)
        queue.put(None)

    userlist = MagicMock()
    userlist.list_profile_spammers = MagicMock(side_effect=list_spammers)

    builder = MagicMock()
    builder.create_from_url = MagicMock(return_value=builder)
    builder.set_readonly = MagicMock(return_value=builder)
    builder.build_user_list.return_value = userlist

    # Override the entry point with our mock
    ds.Builder = builder

    runner = CliRunner()
    with runner.isolated_filesystem():
        write_xdg_token()

        result = runner.invoke(
            damspam_cli,
            ["sanitize-profiles", "--created-after", since],
        )
        logger.debug(result)
        logger.debug(f"Output: {result.output}")
        assert not result.exception
        assert result.exit_code == 0

        builder.create_from_url.assert_called_with(
            "https://example.com/",
            "xdgtokenvalue.spambot.token",
        )
        builder.build_user_list.assert_called()
        userlist.list_profile_spammers.assert_called()

        for user in users:
            mcalls = user.method_calls
            assert (
                len(mcalls) == 1
            ), f"Unexpected method call(s) on user object: {mcalls}"
            # user.sanitize() should be called without args
            name, args, kwargs = mcalls[0]
            assert name == "sanitize"
            assert args == ()
            assert kwargs == {}


@pytest.mark.parametrize("bot", (damspam.SPAMBOT, damspam.BUGBOT))
@pytest.mark.parametrize("state", ("open", "closed", "assigned"))
@patch("damspam.cli.ds")
def test_process_webhook_request(ds, monkeypatch, state, bot):
    monkeypatch.setenv("XDG_CONFIG_HOME", ".")

    builder = MagicMock()
    issue = MagicMock()
    issue.is_closed = state == "closed"
    issue.is_assigned_to = lambda _: state == "assigned"
    builder.create_from_url = MagicMock(return_value=builder)
    builder.set_project = MagicMock(return_value=builder)
    builder.set_issue_iid = MagicMock(return_value=builder)
    builder.set_readonly = MagicMock(return_value=builder)
    builder.build_issue = MagicMock(return_value=issue)

    # Override the entry point with our mock
    ds.Builder = builder

    # these are used internally, make sure they are the correct objects
    ds.SPAMBOT = damspam.SPAMBOT
    ds.BUGBOT = damspam.BUGBOT

    if bot == damspam.SPAMBOT:
        from damspam.cli import damspam as damspam_cli

        cmd = damspam_cli
    elif bot == damspam.BUGBOT:
        from damspam.cli import bugbot as bugbot_cli

        cmd = bugbot_cli
    else:
        assert False, "Unsupported command"

    # Not techically needed since the CLI gets the object from the builder
    # and defaults to a MagicMock but this makes sure we blow up if we
    # somehow get around it
    ds.Project = NonCallableMagicMock()
    runner = CliRunner()
    with runner.isolated_filesystem():
        write_xdg_token()

        url = "https://example.com/foo/bar/-/issues/123"
        result = runner.invoke(
            cmd,
            ["process-webhook-request", url],
        )
        logger.debug(result)
        logger.debug(f"Output: {result.output}")
        if state == "closed":
            assert result.exit_code == 1
            assert "already closed" in result.output
            return
        if state == "assigned":
            assert result.exit_code == 1
            assert "already assigned" in result.output
            return

        assert not result.exception
        assert result.exit_code == 0

        builder.create_from_url.assert_called_with(
            "https://example.com/",
            "xdgtokenvalue.spambot.token",
        )
        builder.build_issue.assert_called()
        issue.process_webhook_request.assert_called_with(bot)
        issue.update_with_message.assert_called()


@pytest.mark.parametrize("instance", ["https://example.com"])
@pytest.mark.parametrize("project", ["foo/bar", "bar/baz/bat"])
@pytest.mark.parametrize("source", ["issues", "merge_requests"])
@pytest.mark.parametrize("readonly", (True, False))
@patch("subprocess.Popen")
def test_bugbot_gitlab_triage(popen, monkeypatch, instance, project, source, readonly):
    with patch("requests.get") as requests_get:
        from damspam.cli import bugbot as bugbot_cli

        separator = "#" if source == "issues" else "!"

        monkeypatch.setenv("XDG_CONFIG_HOME", ".")

        # a fake process to return from popen
        process = MagicMock()
        process.communicate.return_value = (
            "gitlab-triage fake stdout",
            "gitlab-triage fake stderr",
        )
        process.returncode = 0

        def popen_process(*args, **kwargs):
            return process

        popen.side_effect = popen_process

        # Fake statuses to return from requests.get
        @attr.s
        class Status:
            status_code: int = attr.ib()
            text: str = attr.ib()

        codes = (c for c in (404, 200))

        def side_effect(_):
            return Status(status_code=next(codes), text="CONTENT")

        requests_get.side_effect = side_effect

        runner = CliRunner()
        with runner.isolated_filesystem():
            write_xdg_token("user.token", project="bugbot")
            url = f"{instance}/{project}/-/{source}/123"
            args = []
            if readonly:
                args.append("--readonly")
            args += ["gitlab-triage", url]
            result = runner.invoke(bugbot_cli, args, catch_exceptions=False)
            logger.debug(result)
            logger.debug(f"Output: \n{result.output}")

            logger.info(requests_get.calls)
            for branch in ["master", "main"]:
                url = f"{instance}/{project}/-/raw/{branch}/.triage-policies.yml"
                requests_get.assert_any_call(url)
            args = " ".join(popen.call_args.args[0])
            assert args.startswith("gitlab-triage")
            assert f"--host-url {instance}" in args
            assert f"--source-id {project}" in args
            assert f"--resource-reference {separator}123" in args
            assert "--token xdgtokenvalue.user.token" in args
            if readonly:
                assert "--dry-run" in args
