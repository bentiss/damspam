# SPDX-Licences-Identifier: MIT

__version__ = "0.1.0"

from typing import Any, Optional, Union, Type
from datetime import datetime, timedelta, timezone
from pathlib import Path

import attr
import enum
import gitlab
import gitlab.v4.objects
import logging
import queue
import re
import subprocess
import tempfile
import yaml

logging.basicConfig(format="%(levelname)7s| %(name)s: %(message)s")
logger = logging.getLogger("damspam")

GHOST_USER_ID = 2  # @ghost
FDO_BOTS_PROJECT_ID = 20106  # freedesktop/fdo-bots

"""
We only use one label across all projects. No way to change this so we don't
have to worry about different types of spelling/marking spam.
"""
SPAM_LABEL = "Spam"


BOT_MESSAGE = """
I was asked to look at this issue, but it's too complicated for me. I'm just a
simple bot. Please use the Report Abuse to Administrator button instead.
"""
SANITIZED_NOTE_PREFIX = "Profile sanitized by damspam:"

FDO_BOTS_GIT_REPO = "git@gitlab.freedesktop.org:freedesktop/fdo-bots.git"


@attr.s
class Bot:
    id: int = attr.ib()
    username: str = attr.ib()
    label: str = attr.ib()
    template_class: Type["RequestWebhookTemplate"] = attr.ib()
    webhook_url: str = attr.ib(default=None)

    @property
    def project(self) -> str:
        # hookiedookie uses "damspam" and "bugbot" as project names
        # which are also our default labels, so let's keep this simple
        return self.label


@attr.s
class RequestWebhookTemplate:
    project: str = attr.ib()
    project_id: int = attr.ib()
    user_id: int = attr.ib()
    username: str = attr.ib()

    PREFIX = "<undefined>"

    @classmethod
    def title_prefix(cls) -> str:
        return f"{cls.PREFIX} webhook request: "

    @property
    def title(self) -> str:
        return f"{self.title_prefix()}{self.project}"

    @property
    def labels(self) -> str:
        raise NotImplementedError

    @property
    def body(self) -> str:
        raise NotImplementedError

    def as_dict(self) -> dict[str, Any]:
        return {
            "title": self.title,
            "description": self.body,
            "labels": self.labels,
        }

    @classmethod
    def from_gitlab(
        cls, project: gitlab.v4.objects.Project, user: gitlab.v4.objects.CurrentUser
    ) -> "RequestWebhookTemplate":
        return cls(
            project=project.path_with_namespace,
            project_id=project.id,
            user_id=user.id,
            username=user.username,
        )

    @classmethod
    def project_from_title(cls, title: str) -> Optional[str]:
        matches = re.match(
            rf"{cls.title_prefix()}(?P<namespace>[\w_/\-\.]+)/(?P<project>[\w_/\-\.]+)$",
            title,
        )
        if matches:
            try:
                return f"{matches['namespace']}/{matches['project']}"
            except Exception:
                pass
        return None


@attr.s
class DamspamRequestWebhookTemplate(RequestWebhookTemplate):
    PREFIX = "damspam"

    @property
    def labels(self) -> list[str]:
        return ["damspam"]

    @property
    def body(self) -> str:
        return "\n".join(
            (
                f"Please create the required webhooks in {self.project}",
                "to let damspam handle the Spam label.",
                "",
                f"damspam on behalf of @{self.username}",
            )
        )


@attr.s
class BugbotRequestWebhookTemplate(RequestWebhookTemplate):
    PREFIX = "bugbot"

    @property
    def labels(self) -> list[str]:
        return ["bugbot"]

    @property
    def body(self) -> str:
        return "\n".join(
            (
                f"Please create the required webhooks in {self.project}",
                "to let bugbot handle issues and merge requests.",
                "",
                f"bugbot on behalf of @{self.username}",
            )
        )


SPAMBOT = Bot(
    id=85713,
    username="spambot",
    webhook_url="http://damspam-hookiedookie.fdo-bots.svc/webhook",
    label="damspam",
    template_class=DamspamRequestWebhookTemplate,
)
BUGBOT = Bot(
    id=94117,
    username="bugbot",
    webhook_url="http://bugbot-hookiedookie.fdo-bots.svc/webhook",
    label="bugbot",
    template_class=BugbotRequestWebhookTemplate,
)


def not_read_only(ro: Union[Any, bool], message) -> bool:
    """
    Helper function for a read-only context that only executes the condition
    if readonly is `False`. Usage:

    >>> if not_read_only(self.readonly, "Deleting user"):
            user.delete()

    The user is only deleted if `self.readonly` is False.
    """
    if isinstance(ro, bool):
        readonly = ro
    else:
        assert hasattr(ro, "readonly")
        readonly = ro.readonly
    prefix = "[RO SKIP] " if readonly else ""
    logger.info(f"{prefix}{message}")
    return not readonly


def create_bot_note(
    ro: Union[Any, bool], issue: gitlab.v4.objects.ProjectIssue, log_message: str
):
    """
    If not in readonly mode, create a note with the standard bot message.
    """
    if not_read_only(ro, log_message):
        issue.notes.create({"body": BOT_MESSAGE})


@attr.s
class ProtectedUser:
    id: int = attr.ib()
    name: str = attr.ib()

    @classmethod
    def from_bot(cls, bot: Bot) -> "ProtectedUser":
        return cls(id=bot.id, name=bot.username)


@attr.s
class ProtectedUserList:
    """This is a singleton class"""

    _protected_users: dict[int, ProtectedUser] = attr.ib()

    _instance: "ProtectedUserList" = None  # type: ignore

    @classmethod
    def instance(cls) -> "ProtectedUserList":
        if not cls._instance:
            cls._instance = ProtectedUserList(
                protected_users={  # type: ignore
                    u.id: u
                    for u in [
                        ProtectedUser(id=GHOST_USER_ID, name="ghost"),
                        ProtectedUser.from_bot(SPAMBOT),
                    ]
                },
            )
        return cls._instance

    @property
    def ghost(self) -> ProtectedUser:
        return self._protected_users[GHOST_USER_ID]

    @property
    def spambot(self) -> ProtectedUser:
        return self._protected_users[SPAMBOT.id]

    def lookup_user_by_id(self, id: int) -> Optional[ProtectedUser]:
        return self._protected_users.get(id, None)

    def lookup_user_by_name(self, name: str) -> Optional[ProtectedUser]:
        try:
            return next(
                filter(
                    lambda u: u is not None and u.name == name,
                    self._protected_users.values(),
                )
            )
        except StopIteration:
            return None

    def is_protected(self, id: int) -> bool:
        return id in self._protected_users


@attr.s
class BuilderError(Exception):
    message: str = attr.ib()

    def __str__(self):
        return f"Builder Error: {self.message}"


@attr.s
class Builder:
    """
    Builder class for DamSpam. Use this to set the fields required for
    a specific
    """

    gl: gitlab.Gitlab = attr.ib()  # type: ignore
    project: Optional[gitlab.v4.objects.Project] = attr.ib(default=None)
    issue: Optional[gitlab.v4.objects.ProjectIssue] = attr.ib(init=False, default=None)
    user: Optional[gitlab.v4.objects.User] = attr.ib(init=False, default=None)
    tracker_issue: Optional[gitlab.v4.objects.ProjectIssue] = attr.ib(
        init=False, default=None
    )
    # We default to readonly true so a runaway bot can't change anything and any copies of
    # this object explicitly needs to disable readonly
    readonly: bool = attr.ib(init=False, default=True)

    @classmethod
    def create_from_url(cls, url: str, private_token: str) -> "Builder":
        logger.debug(f"Using GitLab instance at: {url}")
        gl = gitlab.Gitlab(url=url, private_token=private_token)  # type: ignore
        gl.auth()
        return cls(gl=gl)

    @classmethod
    def create_from_instance(
        cls,
        gl: gitlab.Gitlab,  # type: ignore
    ) -> "Builder":
        return cls(gl=gl)

    @classmethod
    def create_from_project(
        cls, gl: gitlab.Gitlab, project: gitlab.v4.objects.Project  # type: ignore
    ) -> "Builder":
        return cls(gl=gl, project=project)

    def set_project(self, full_name: str) -> "Builder":
        self.project = self.gl.projects.get(id=full_name)
        if self.project is None:
            raise BuilderError(f"Project {full_name} not found")
        logger.debug(f"Using GitLab project: {full_name}")
        return self

    def set_project_id(self, id: int) -> "Builder":
        self.project = self.gl.projects.get(id=id)
        if self.project is None:
            raise BuilderError("Project {id} not found")
        logger.debug(f"Using GitLab project: {self.project.path_with_namespace}")
        return self

    def set_issue_iid(self, issue_iid: int) -> "Builder":
        logger.debug(f"Using GitLab project issue: #{issue_iid}")
        if self.project is None:
            raise BuilderError("Cannot fetch an issue without a project")
        issue = self.project.issues.get(id=issue_iid)
        if issue is None:
            raise BuilderError("Project issue {issue_iid} not found")
        if issue == self.tracker_issue:
            raise BuilderError("Tracker issue must differ from issue")
        self.issue = issue
        return self

    def set_username(self, username: str) -> "Builder":
        logger.debug(f"Using GitLab username: {username}")
        for user in self.gl.users.list(iterator=True, search=username):
            if user.username == username:
                user = self.gl.users.get(user.id)
                self.user = user
                break
        else:
            raise BuilderError(f"Unable to find user {username}")
        return self

    def set_user_id(self, id: int) -> "Builder":
        logger.debug(f"Using GitLab user id: {id}")
        self.user = self.gl.users.get(id)
        return self

    def set_readonly(self, readonly: bool) -> "Builder":
        self.readonly = readonly
        return self

    def set_tracker_project_issue(
        self, namespace: str, project: str, iid: int
    ) -> "Builder":
        tracker_project = self.gl.projects.get(id=f"{namespace}/{project}")
        if tracker_project is None:
            raise BuilderError("Tracker project '{namespace}/{project}' not found")
        tracker_issue = tracker_project.issues.get(id=iid)
        if tracker_issue is None:
            raise BuilderError("Tracker issue '{namespace}/{project}#{iid}' not found")

        if self.issue == tracker_issue:
            raise BuilderError("Tracker issue must differ from issue")

        self.tracker_issue = tracker_issue

        logger.debug(f"Using tracker issue: {namespace}/{project}#{iid}")
        return self

    def set_tracker_issue(
        self, tracker_issue: gitlab.v4.objects.ProjectIssue
    ) -> "Builder":
        if self.issue == tracker_issue:
            raise BuilderError("Tracker issue must differ from issue")
        self.tracker_issue = tracker_issue
        return self

    def build_issue(self) -> "Issue":
        if any([x is None for x in (self.project, self.issue, self.gl)]):
            raise BuilderError("Cannot build without all fields set")
        # to shut up mypy:
        assert self.gl is not None
        assert self.project is not None
        assert self.issue is not None
        return Issue(
            gl=self.gl,
            project=self.project,
            issue=self.issue,
            readonly=self.readonly,
            tracker_issue=self.tracker_issue,
        )

    def build_user_list(self) -> "UserList":
        if any([x is None for x in (self.gl,)]):
            raise BuilderError("Cannot build without all fields set")
        # to shut up mypy:
        assert self.gl is not None
        return UserList(
            gl=self.gl,
            readonly=self.readonly,
        )

    def build_user(self) -> "User":
        if any([x is None for x in (self.gl, self.user)]):
            raise BuilderError("Cannot build without all fields set")

        # to shut up mypy:
        assert self.gl is not None
        assert self.user is not None

        # mypy doesn't like list()
        issues = []
        for i in self.gl.issues.list(
            author_id=self.user.id,
            scope="all",
            iterator=True,
        ):
            issues.append(i)

        return User(
            gl=self.gl,
            user=self.user,
            issues=issues,
            readonly=self.readonly,
        )

    def build_project(self) -> "Project":
        if any([x is None for x in (self.gl, self.project)]):
            raise BuilderError("Cannot build without all fields set")

        # to shut up mypy:
        assert self.gl is not None
        assert self.project is not None

        user = self.gl.user
        assert user is not None
        return Project(
            gl=self.gl,
            project=self.project,
            user=user,
            readonly=self.readonly,
        )


class SafetyFlags(enum.Flag):
    """
    Flags used when hiding an issue. If a flag is enabled (usually all are)
    the respective feature *prevents* the issue from being hidden. See the
    block_issue_creator code for details.
    """

    ALREADY_ASSIGNED = enum.auto()  # already assigned to spambot
    ALREADY_BLOCKED = enum.auto()  # user is already blocked
    ACCOUNT_AGE = enum.auto()  # user account is too old
    CONVERSATION_LENGTH = enum.auto()  # to many ppl conversing in the comment

    @classmethod
    def all(cls) -> "SafetyFlags":
        return ~SafetyFlags(0)

    @classmethod
    def ignore(cls, flags: "SafetyFlags") -> "SafetyFlags":
        """
        Return all safety flags except the ones given in the argument.
        """
        return SafetyFlags.all() ^ flags


@attr.s
class Issue:
    gl: gitlab.Gitlab = attr.ib()  # type: ignore
    project: gitlab.v4.objects.Project = attr.ib()
    issue: gitlab.v4.objects.ProjectIssue = attr.ib()
    tracker_issue: Optional[gitlab.v4.objects.ProjectIssue] = attr.ib(default=None)
    readonly: bool = attr.ib(default=False)

    @property
    def is_closed(self) -> bool:
        return self.issue.state == "closed"

    def is_assigned_to(self, bot: int) -> bool:
        return bot in [a["id"] for a in self.issue.assignees]

    def block_issue_creator(
        self,
        recursive: bool = True,
        safety_flags: SafetyFlags = SafetyFlags.all(),
    ) -> None:
        """
        Block the user that created the issue in this DamSpam instance. If
        recursive is True, all issues opened by them are tagged as "Spam", made
        confidential and assigned to the spambot.
        """

        protected_user = ProtectedUserList.instance().lookup_user_by_id(
            self.issue.assignee
        )
        if protected_user is not None:
            logger.info(f"Issue already assigned to {protected_user.name}")
            if SafetyFlags.ALREADY_ASSIGNED in safety_flags:
                return

        if self.issue.author["state"] != "active":
            if SafetyFlags.ALREADY_BLOCKED in safety_flags:
                create_bot_note(
                    self,
                    self.issue,
                    f"Author '{self.issue.author['name']}' is already {self.issue.author['state']}",
                )
                return

        # Safety check: if we have more than 2 commenters on the issue, we
        # say we don't know what to do.
        commenters = {n.author["id"] for n in self.issue.notes.list()}
        if len(commenters) > 2:
            if SafetyFlags.CONVERSATION_LENGTH in safety_flags:
                create_bot_note(
                    self,
                    self.issue,
                    "Three or more people involved, please report directly",
                )
                return

        # Block the user first, ideally that way they don't get notified about the rest of the
        # work we do here.
        try:
            spammer = self.gl.users.get(self.issue.author["id"])
        except gitlab.exceptions.GitlabGetError as e:
            logger.error(
                f"Failed to look up spammer {self.issue.author['id']} by id: {e}"
            )
            return

        # Safety check: if the profile is more than 6 months old, don't mark them
        # as spammer. This is mostly to save us from accidentally marking a long-term
        # contributor as spammer and then having to manually undo hundreds of issues.
        try:
            created_at = datetime.fromisoformat(spammer.created_at)
            now = datetime.now(tz=timezone.utc)
            dt = timedelta(weeks=26)
            if created_at < now - dt:
                if SafetyFlags.ACCOUNT_AGE in safety_flags:
                    create_bot_note(
                        self,
                        self.issue,
                        f"Account of alleged spammer {spammer.name} is more than 6m old, skipping",
                    )
                    return
        except (ValueError, TypeError) as e:
            logger.error(f"Unable to parse created_at from spammer: {e}")

        if not_read_only(self, f"Blocking user '{spammer.name}'"):
            spammer.block()

        self.mark_as_spam()
        self.link_to_tracker()

        if recursive:
            # find other issues by this author, mark them all as spam.
            # Python gitlab has Issues and ProjectIssues, only the latter can be edited.
            issues_to_block = self.gl.issues.list(
                author_id=self.issue.author["id"],
                scope="all",
                iterator=True,  # handles pagination for us
            )
            for issue in filter(lambda i: i.id != self.issue.id, issues_to_block):
                if issue.project_id != self.project.id:
                    project = self.gl.projects.get(issue.project_id)
                    if project is None:
                        logger.warning(
                            "Unable to access project {issue.project_id} for recursive block"
                        )
                        continue
                    builder = Builder.create_from_project(self.gl, project)
                else:
                    builder = Builder.create_from_project(self.gl, self.project)

                builder.set_issue_iid(issue.iid).set_readonly(self.readonly)

                try:
                    if self.tracker_issue:
                        builder.set_tracker_issue(self.tracker_issue)
                except BuilderError:
                    pass  # Issue == tracker_issue, can't have that
                else:
                    damspam = builder.build_issue()
                    damspam.mark_as_spam()
                    damspam.link_to_tracker()

    def mark_as_spam(self):
        """
        Labels a single issue as spam, marks it confidential and assigns it to the spambot.
        """
        assert self.tracker_issue != self.issue

        if not_read_only(
            self,
            f"Processing spam issue {self.issue.id} ({self.project.name}#{self.issue.iid}): {self.issue.title}",
        ):
            labels = self.issue.labels
            self.issue.labels = [SPAM_LABEL] + labels
            self.issue.confidential = True
            self.issue.assignee_ids = [SPAMBOT.id]
            self.issue.state_event = "close"
            self.issue.save()

    def link_to_tracker(self):
        """
        Links this issue to the spam tracker issue
        """
        if not self.tracker_issue:
            logger.warning("No tracker issue set, cannot link")
            return

        assert self.tracker_issue != self.issue

        if not_read_only(
            self,
            f"Linking to '{self.tracker_issue.title}': {self.issue.id} ({self.project.name}#{self.issue.iid}): {self.issue.title}",
        ):
            data = {
                "target_project_id": self.tracker_issue.project_id,
                "target_issue_iid": self.tracker_issue.iid,
            }
            self.issue.links.create(data)

    def update_with_message(
        self,
        message: str,
        close: bool,
        label_prefix: str,
        assign_to: Optional[int] = None,
        is_success: Optional[bool] = None,
    ):
        """
        Update the issue with the message and optionally closing it.
        This also assigns the label_prefix label and if is_succcess is True
        or False (but not None), the label_prefix::success/failed label.
        """
        if not_read_only(self, message):
            if assign_to is not None:
                self.issue.assignee_ids = [assign_to]
            labels = self.issue.labels + [label_prefix]
            labels += {
                True: [f"{label_prefix}::success"],
                False: [f"{label_prefix}::failed"],
                None: [],
            }[is_success]
            self.issue.labels = labels
            self.issue.notes.create({"body": message})
            if close:
                self.issue.state_event = "close"
            self.issue.save()

    def process_webhook_request(self, bot: Bot) -> Optional[str]:
        from uuid import uuid4

        project_name = bot.template_class.project_from_title(self.issue.title)
        if not project_name:
            return "Failed to extract project name"

        if self.is_assigned_to(bot.id):
            return f"Issue already assigned to @{bot.username}"

        try:
            project = self.gl.projects.get(id=project_name)
            author = self.issue.author
            if not self.gl.users.get(author["id"]).is_admin:
                member = project.members_all.get(author["id"])
                if member.access_level < gitlab.const.AccessLevel.MAINTAINER:
                    return "Webhooks must be requested by a user with role Maintainer or above in this project"
        except gitlab.exceptions.GitlabGetError:
            return "Unable to check the project membership"

        token = str(uuid4())

        try:
            with Secrets.clone(bot, readonly=self.readonly) as secrets:
                secrets.add_project_entry(project_name, token)
                secrets.push()
        except SecretsUpdateError as e:
            return e.message

        url = f"{bot.webhook_url}/{project_name}"
        ds_project = Project(
            gl=self.gl, project=project, user=None, readonly=self.readonly
        )
        result = ds_project.add_project_webhook(url=url, token=token)

        return result


@attr.s
class SecretsUpdateError(Exception):
    message: str = attr.ib()


@attr.s
class Secrets:
    """
    Abstracts the handling of the helm-gitlab-secrets git repo. This is a submodule
    inside the fdo-bots module.
    """

    repo_path: Path = attr.ib()  # abs repo path
    bot: Bot = attr.ib()  # one of damspam/bugbot
    submodule_path: Path = attr.ib()  # submodule path relative to repo_path
    readonly: bool = attr.ib(default=True)
    damspam_config: Path = attr.ib(
        init=False
    )  # damspam.yaml relative to submodule_path
    _tmpdir: Optional[tempfile.TemporaryDirectory] = attr.ib(default=None)

    @damspam_config.default  # type: ignore
    def _default_damspam_config(self):
        return Path(f"{self.bot.project}.yaml")

    @submodule_path.validator  # type:ignore
    def _validate_submodule_path(self, attribute, submodule_path):
        path = self.repo_path / submodule_path / self.damspam_config
        if not path.exists():
            raise ValueError(f"Invalid submodule path, missing {self.damspam_config}")

    @damspam_config.validator  # type: ignore
    def _validate_damspam_config(self, attr, config_file):
        # verify the format is still what we expect
        with open(self.repo_path / self.submodule_path / config_file) as fd:
            yml = yaml.safe_load(fd)
            for hook in yml[self.bot.project]["webhooks"]:
                assert hook["path"] is not None
                assert hook["token"] is not None

    def __enter__(self) -> "Secrets":
        return self

    def __exit__(self, *args):
        if self._tmpdir is not None:
            self._tmpdir.cleanup()

    def git_cmds(self, commands: list[list[str]]):
        for cmd in commands:
            logger.debug(f"git command: {cmd}")
            subprocess.run(cmd, cwd=self.repo_path, check=True)

    def git_cmds_submodule(self, commands: list[list[str]]):
        for cmd in commands:
            logger.debug(f"git command: {cmd}")
            subprocess.run(cmd, cwd=self.repo_path / self.submodule_path, check=True)

    def add_project_entry(self, project: str, token: str):
        """
        Add and commit a new entry to the webhooks
        """
        try:
            # We write this out directly instead of using pyyaml because there
            # may be comments or other fields we don't want to mess up
            with open(
                self.repo_path / self.submodule_path / self.damspam_config, "a"
            ) as fd:
                fd.write(
                    "\n".join(
                        (
                            f"    - path: {project}",
                            f"      token: {token}",
                            "",
                        )
                    )
                )
                fd.flush()

            # Re-read immediately to make sure we didn't break the format
            with open(self.repo_path / self.submodule_path / self.damspam_config) as fd:
                data = fd.read()
                yml = yaml.safe_load(data)
                hook = yml[self.bot.project]["webhooks"][-1]
                assert hook["path"] == project
                assert hook["token"] == token

            logger.debug(f"Updating {self.damspam_config} succeeded")

            author = f"freedesktop.org {self.bot.username} <{self.bot.username}@fdo>"

            self.git_cmds_submodule(
                [
                    [
                        "git",
                        "config",
                        "--local",
                        "user.name",
                        f"freedesktop.org {self.bot.username}",
                    ],
                    [
                        "git",
                        "config",
                        "--local",
                        "user.email",
                        f"{self.bot.username}@fdo",
                    ],
                    [
                        "git",
                        "commit",
                        f"--author='{author}'",
                        "-m",
                        f"Add {self.bot.project} webhook: {project}",
                        self.damspam_config,  # type: ignore
                    ],
                ]
            )
            # Now update the submodule in the parent
            self.git_cmds(
                [
                    [
                        "git",
                        "config",
                        "--local",
                        "user.name",
                        f"freedesktop.org {self.bot.username}",
                    ],
                    [
                        "git",
                        "config",
                        "--local",
                        "user.email",
                        f"{self.bot.username}@fdo",
                    ],
                    [
                        "git",
                        "commit",
                        f"--author='{author}",
                        "-m",
                        f"Update for {self.bot.project} webhook: {project}",
                        self.submodule_path,  # type: ignore
                    ],
                ]
            )
        except Exception as e:
            logger.error(e)
            raise SecretsUpdateError(f"Failed to update {self.bot.project} config")

    def push(self):
        try:
            if not_read_only(self, "Pushing changes"):
                self.git_cmds_submodule([["git", "push", "origin", "master"]])
                self.git_cmds([["git", "push", "origin", "main"]])
        except Exception as e:
            logger.error(e)
            raise SecretsUpdateError(f"Failed to push {self.bot.project} config")

    @classmethod
    def clone(
        cls,
        bot: Bot,
        repo_url: str = FDO_BOTS_GIT_REPO,
        readonly=True,
    ) -> "Secrets":
        """
        Clone the freedesktop/fdo-bots repository and set it up with its submodules.

        This works in a temporary directory that is cleaned up on __exit__(), so use
        this with a context manager.
        """
        repo_path = Path("fdo-bots.git")
        submodule_path = Path("helm-gitlab-secrets")

        try:
            tmpdir = tempfile.TemporaryDirectory(ignore_cleanup_errors=True)
            root_path = Path(tmpdir.name)
            # For testing we need to explicitly allow local file clones, otherwise
            # git blocks us from submodules that reference local trees
            allow_file_clone = []
            if str != "SECRETS_GIT_REPO":
                allow_file_clone = ["-c", "protocol.file.allow=always"]

            # check out fdo-bots
            subprocess.run(
                ["git", "clone", "--quiet", repo_url, repo_path],
                cwd=root_path,
                check=True,
            )
            # check out submodules
            subprocess.run(
                ["git", *allow_file_clone, "submodule", "update", "--init"],
                cwd=root_path / repo_path,
                check=True,
            )
            # force submodule to master so we don't end up with a detached head
            subprocess.run(
                ["git", "checkout", "master"],
                cwd=root_path / repo_path / submodule_path,
                check=True,
            )

            return cls(
                repo_path=root_path / repo_path,
                bot=bot,
                submodule_path=submodule_path,
                tmpdir=tmpdir,  # type: ignore
                readonly=readonly,
            )
        except Exception as e:
            logger.error(e)
            raise SecretsUpdateError(f"Cannot load or parse {bot.project} config")


@attr.s
class User:
    gl: gitlab.Gitlab = attr.ib()  # type: ignore
    user: gitlab.v4.objects.User = attr.ib()
    issues: list[gitlab.v4.objects.Issue] = attr.ib(
        default=attr.Factory(list)
    )  # Note: Issue, not a ProjectIssue
    readonly: bool = attr.ib(default=False)

    def delete(self):
        protected_user = ProtectedUserList.instance().lookup_user_by_id(self.user.id)
        if protected_user is not None:
            logger.error(f"User @{protected_user.name} cannot be deleted.")
            return

        if self.user.is_admin:
            logger.error("Cannot remove Administrators. I'm too scared of them.")
            return

        if not_read_only(
            self, f"Deleting user {self.user.username} ({self.user.name})"
        ):
            try:
                self.user.delete(hard_delete=True)
            except gitlab.exceptions.GitlabDeleteError as e:
                # if the user is already deleted, continue with success
                if e.response_code != 404:
                    raise e

    def sanitize(self, only_external: bool = True, force=False):
        protected_user = ProtectedUserList.instance().lookup_user_by_id(self.user.id)
        if protected_user is not None:
            logger.error(f"User @{protected_user.name} cannot be modified.")
            return

        if self.user.is_admin:
            logger.error("Cannot modify Administrators. I'm too scared of them.")
            return

        if only_external and not self.user.external:
            logger.info("Not modifying internal users as requested.")
            return

        # This is the list of things the spammers tend to fill out. If any
        # of those are set, reset everything in the bio
        if any(
            [
                force,
                self.user.bio,
                self.user.twitter,
                self.user.linkedin,
                self.user.website_url,
            ]
        ):
            if not_read_only(
                self,
                f"Sanitizing user {self.user.username} ({self.user.id}) bio and other data",
            ):
                self.user.note = f"{SANITIZED_NOTE_PREFIX} website was '{self.user.website_url}', bio was '{self.user.bio}'"
                try:
                    import urllib.request

                    # This is the avatar gilab uses in its API documentation
                    # We could create our own instead, but this is good enough for now
                    self.user.avatar = urllib.request.urlopen(
                        "https://www.gravatar.com/avatar/7955171a55ac4997ed81e5976287890a?s=80&d=identicon"
                    ).read()
                except Exception as e:
                    logger.error(f"Failed to set user avatar: {e}")
                self.user.bio = ""
                self.user.discord = ""
                self.user.linkedin = ""
                self.user.location = ""
                self.user.twitter = ""
                self.user.website_url = ""
                self.user.save()


@attr.s
class UserList:
    gl: gitlab.Gitlab = attr.ib()  # type: ignore
    readonly: bool = attr.ib(default=False)

    def list_spammers(self) -> list[User]:
        spammers: list[User] = []
        for user in self.gl.users.list(blocked=True, iterator=True):
            logger.debug(f"Checking blocked user {user.username:25s} ({user.name})")
            issues = list(
                self.gl.issues.list(
                    author_id=user.id,
                    labels=f"{SPAM_LABEL}",
                    scope="all",
                    iterator=True,
                )
            )
            if issues:
                spammers.append(
                    User(
                        gl=self.gl,
                        user=user,  # type: ignore
                        issues=issues,  # type: ignore
                        readonly=self.readonly,
                    )
                )

        return spammers

    def list_profile_spammers(
        self,
        queue: queue.Queue,
        cutoff: Optional[datetime] = None,
        include_sanitized_users=False,
    ) -> None:
        for user in self.gl.users.list(
            per_page=100,
            iterator=True,
            order_by="created_at",
            sort="desc",
            without_projects=True,
        ):
            if user.external:
                logger.debug(
                    f"Checking user created on {user.created_at} username {user.username}"
                )
                if any(
                    [
                        user.bio,
                        user.website_url,
                        include_sanitized_users
                        and SANITIZED_NOTE_PREFIX in (user.note or ""),
                    ]
                ):
                    queue.put(
                        User(
                            gl=self.gl,
                            user=self.gl.users.get(user.id),
                            readonly=self.readonly,
                        )
                    )

            if cutoff:
                ts = datetime.fromisoformat(user.created_at)
                if ts < cutoff:
                    break

        # Push a None value to the queue as a signal that we're done
        queue.put(None)


@attr.s
class Project:
    gl: gitlab.Gitlab = attr.ib()  # type: ignore
    project: gitlab.v4.objects.Project = attr.ib()
    user: Optional[gitlab.v4.objects.CurrentUser] = attr.ib()
    readonly: bool = attr.ib(default=False)

    def user_is_maintainer(self) -> bool:
        assert self.user is not None
        try:
            if self.gl.users.get(self.user.id).is_admin:
                return True
            member = self.project.members_all.get(self.user.id)
            return member.access_level >= gitlab.const.AccessLevel.MAINTAINER
        except gitlab.exceptions.GitlabGetError:
            return False

    def file_webhook_request_issue(self, template: RequestWebhookTemplate) -> str:
        assert self.user is not None
        try:
            fdo_bots_project = self.gl.projects.get(id=FDO_BOTS_PROJECT_ID)
        except gitlab.exceptions.GitlabGetError:
            assert False, "Unable to find the fdo_bots_project. This shouldn't happen"

        if not_read_only(self, f"Filing issue: {template.title}"):
            issue = fdo_bots_project.issues.create(template.as_dict())
            return issue.web_url
        else:
            return "Nothing to do, readonly requested"

    def add_project_webhook(self, url: str, token: str) -> Optional[str]:
        for hook in self.project.hooks.list(iterator=True):
            if hook.url == url:
                return "Hook already exists"

        if not_read_only(self, f"Adding issue event webhook to {url}"):
            hook = self.project.hooks.create(
                {
                    "url": url,
                    "token": token,
                    "push_events": False,
                    "issues_events": True,
                    "confidential_issues_events": True,
                    "merge_requests_events": True,
                    "enable_ssl_verification": False,
                }
            )
            logger.debug(hook)

        return None
