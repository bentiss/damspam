# SPDX-Licences-Identifier: MIT
#
# This file is formatted with Python Black

from typing import Optional, Tuple, Type
from pathlib import Path

import attr
import click
import datetime
import logging
import os
import queue
import re
import tempfile
import threading
import requests
import subprocess
import time

import damspam as ds

logger = logging.getLogger("damspam")

DEFAULT_INSTANCE = "https://gitlab.freedesktop.org"


def xdg_config_home() -> Path:
    return Path(os.environ.get("XDG_CONFIG_HOME", Path.home() / ".config"))


def xdg_token(name: str = "spambot.token", project: str = "damspam") -> Optional[str]:
    xdg_path = xdg_config_home() / project / name
    try:
        token = open(xdg_path).read().rstrip()
    except FileNotFoundError:
        return None
    return token


def die(message: str):
    click.echo(message)
    raise SystemExit(1)


@attr.s
class Project:
    path_with_namespace: str = attr.ib()
    instance: str = attr.ib(default=DEFAULT_INSTANCE)

    @classmethod
    def from_url(cls, url: str) -> Optional["Project"]:
        matches = re.match(
            r"(?P<instance>http[s]?://[^/]+)/(?P<project>[\w_/-]+)/?",
            url,
        )
        if not matches:
            return None

        return cls(instance=matches["instance"], path_with_namespace=matches["project"])

    @classmethod
    def from_spec(cls, spec: str) -> Optional["Project"]:
        # should probably do some validation here...
        return cls(path_with_namespace=spec)


@attr.s
class Issue:
    project: Project = attr.ib()
    iid: int = attr.ib()

    @property
    def separator(self) -> str:
        return "#"

    @classmethod
    def from_url(cls, url: str) -> Optional["Issue"]:
        match = re.match(
            r"(?P<instance>http[s]?://[^/]+)/(?P<project>[\w_/-]+)/-/issues/(?P<issue>[0-9]+)",
            url,
        )
        try:
            instance, project, issue_iid = match["instance"], match["project"], int(match["issue"])  # type: ignore
            return Issue(
                project=Project(instance=instance, path_with_namespace=project),
                iid=issue_iid,
            )
        except (TypeError, IndexError, ValueError):
            return None

    @classmethod
    def from_spec(cls, spec: str) -> Optional["Issue"]:
        try:
            splits = spec.split("#")
            project, issue_iid = splits[0], int(splits[1])
            return cls(project=Project(path_with_namespace=project), iid=issue_iid)
        except (IndexError, ValueError):
            return None


@attr.s
class MergeRequest:
    project: Project = attr.ib()
    iid: int = attr.ib()

    @property
    def separator(self) -> str:
        return "!"

    @classmethod
    def from_url(cls, url: str) -> Optional["MergeRequest"]:
        match = re.match(
            r"(?P<instance>http[s]?://[^/]+)/(?P<project>[\w_/-]+)/-/merge_requests/(?P<merge_request>[0-9]+)",
            url,
        )
        try:
            instance, project, merge_request_iid = match["instance"], match["project"], int(match["merge_request"])  # type: ignore
            return MergeRequest(
                project=Project(instance=instance, path_with_namespace=project),
                iid=merge_request_iid,
            )
        except (TypeError, IndexError, ValueError):
            return None

    @classmethod
    def from_spec(cls, spec: str) -> Optional["MergeRequest"]:
        try:
            splits = spec.split("!")
            project, merge_request_iid = splits[0], int(splits[1])
            return cls(
                project=Project(path_with_namespace=project), iid=merge_request_iid
            )
        except (IndexError, ValueError):
            return None


@attr.s
class Config:
    instance: str = attr.ib()
    verbose: bool = attr.ib()
    readonly: bool = attr.ib()
    token: Optional[str] = attr.ib()


@attr.s
class Params:
    config: Config = attr.ib()
    instance: str = attr.ib()
    token: str = attr.ib()

    @instance.validator  # type: ignore
    def _validate_instance(self, attribute, value):
        if value is None:
            raise ValueError


class GitlabIssueSpec(click.ParamType):
    name = "GitLab issue <namespace/name#iid>"

    def convert(self, value, param, ctx):
        try:
            path, issue = value.split("#")
            ns, name = path.split("/")
            iid = int(issue, 10)
            return (ns, name, iid)
        except ValueError:
            self.fail(f"{value!r} is not a valid issue spec", param, ctx)


@click.group()
@click.option("--verbose", "-v", is_flag=True, help="Enable debug logging")
@click.option("--readonly", is_flag=True, help="Print, but don't modify")
@click.option(
    "--token-file",
    type=click.Path(exists=True, dir_okay=False),
    help="File containing the GitLab API token. Defaults to XDG_CONFIG_HOME/damspam/spambot.token",
)
@click.option(
    "--token-env",
    type=str,
    help="Environment variable containing the GitLab API token",
)
@click.option(
    "--instance",
    type=str,
    help=f"The GitLab instance (default: {DEFAULT_INSTANCE}",
)
@click.pass_context
def damspam(
    ctx,
    verbose: bool,
    readonly: bool,
    token_file: Optional[click.Path],
    token_env: Optional[str],
    instance: Optional[str],
):
    global logger
    """
    Identify a spammer and pseudo-remove them from gitlab. Currently this works by
    taking an issue, then blocking the creater of that issue and hiding all issues
    filed by the same user.

    This tool does *not* permanently ban the user, doing so requires manual effort
    by a GitLab administrator. Instead, by blocking the user and hiding all issues
    it intends to make GitLab spam less useful.

    This tool is intended to be passed a GitLab issue-event webhook payload, see
    'damspam issue-hook --help'. It can be run manually with the 'issue' command.
    The functionality is largely identical.
    """
    logger.setLevel(logging.DEBUG if verbose else logging.INFO)

    if token_file is not None:
        token = open(token_file).read().rstrip() or die("Empty token file")  # type: ignore
    elif token_env is not None:
        token = os.environ.get(token_env) or die(
            "Requested token environment variable does not exist, use --token-env"
        )
    else:
        token = None

    # Note: this not a click default so we can patch it in the tests
    instance = instance or DEFAULT_INSTANCE
    ctx.obj = Config(instance=instance, verbose=verbose, readonly=readonly, token=token)


@damspam.command(name="hide-issue", help="Process the given GitLab spam issue")
@click.option(
    "--recursive/--no-recursive",
    default=True,
    help="Recursively hide all issues by the user that created this issue",
)
@click.option(
    "--tracker-issue",
    type=GitlabIssueSpec(),
    help="The spam tracker issue to link this issue with",
    default="freedesktop/freedesktop#548",
)
@click.option(
    "--skip-tracker-issue",
    type=bool,
    is_flag=True,
    help="Skip linking to the tracker issue",
)
@click.option(
    "--ignore",
    multiple=True,
    default=[],
    type=click.Choice(
        ["already-assigned", "already-blocked", "account-age", "conversation-length"]
    ),
    help="Ignore the given safety check and proceed anyway. May be passed multiple times",
)
@click.argument(
    "issue",
    type=str,
)
@click.pass_context
def do_hide_issue(
    ctx,
    recursive: bool,
    tracker_issue: Tuple[str, str, int],
    skip_tracker_issue: bool,
    issue: str,
    ignore: Tuple[str, ...],
):
    """
    Hide the given issue and block the creating user as well as (if operating recursively)
    hiding all issues created by the same user.

    The issue is either in the form of "namespace/project#123" or as a URL to the issue, i.e.
    https://gitlab.freedesktop.org/namespace/project/-/issues/123
    The host name must match the --instance argument.

    If ignore flags are given:
    - "already-assigned" proceeds even if the issue is already assigned to spambot
    - "already-blocked" proceeds even if the issue creator is already blocked
    - "account-age" proceeds even if the issue creator's account is older than expected
    - "conversation-length" proceeds even if there are multiple users in the conversation
    """
    config = ctx.obj
    instance = config.instance
    if issue.startswith("https://"):
        if not issue.startswith(instance):
            die("Invalid URL, --instance arg required")

        iss = Issue.from_url(issue)
    else:
        iss = Issue.from_spec(issue)

    if not iss:
        die(f"Failed to parse issue {issue}")

    token = (
        config.token
        or xdg_token()
        or die("One of --token-file or --token-env is required")
    )
    builder = (
        ds.Builder.create_from_url(instance, token)
        .set_project(iss.project.path_with_namespace)
        .set_issue_iid(iss.iid)
        .set_readonly(config.readonly)
    )

    flags = ds.SafetyFlags(0)
    for flag in ignore:
        flags |= {
            "already-assigned": ds.SafetyFlags.ALREADY_ASSIGNED,
            "already-blocked": ds.SafetyFlags.ALREADY_BLOCKED,
            "account-age": ds.SafetyFlags.ACCOUNT_AGE,
            "conversation-length": ds.SafetyFlags.CONVERSATION_LENGTH,
        }[flag]

    safety_flags = ds.SafetyFlags.ignore(flags)

    if not skip_tracker_issue:
        (ns, name, iid) = tracker_issue
        builder = builder.set_tracker_project_issue(ns, name, iid)
    dam = builder.build_issue()
    dam.block_issue_creator(recursive=recursive, safety_flags=safety_flags)


@damspam.command(name="purge-spammers", help="Purge (delete) any spammers")
@click.option(
    "--show-all-issues",
    is_flag=True,
    help="Show all spam issues by this user, not just the first",
)
@click.pass_context
def do_purge_spammers(
    ctx,
    show_all_issues: bool,
):
    """
    List all blocked users that have at least one issue with the Spam label applied.
    Then interactively delete one or more users in that list.
    """
    config = ctx.obj
    token = (
        config.token
        or xdg_token()
        or die("One of --token-file or --token-env is required")
    )
    instance = config.instance
    builder = ds.Builder.create_from_url(instance, token).set_readonly(config.readonly)

    dam = builder.build_user_list()
    spammers = dam.list_spammers()
    if not spammers:
        click.echo("No spammers or candidates found. Yay?")
        raise SystemExit(0)

    for idx, spammer in enumerate(spammers):
        user = spammer.user
        issue = spammer.issues[0]
        click.echo(f"{idx:3d}: {user.username:25s}: {issue.web_url}: {issue.title}")
        if show_all_issues:
            for issue in spammer.issues[1:]:
                click.echo(f"{'':3s}  {'':25s}  {issue.web_url}: {issue.title}")

    click.secho(
        "Purging a user means a full delete including all issues, MRs, etc. This is nonrecoverable!",
        bold=True,
    )
    click.echo("Please select the users to purge:")

    while True:
        value: str = click.prompt("[q]uit, purge [a]ll, or the index", type=str)
        if value == "q":
            break

        try:
            idx = int(value)
            if idx >= len(spammers):
                click.secho(f"Invalid index {idx}")
                continue

            spammers[idx].delete()
            continue
        except ValueError:
            if value == "a":
                for s in spammers:
                    s.delete()
            raise SystemExit(0)


@damspam.command(name="purge-user", help="Purge (delete) a given user")
@click.argument(
    "username",
    type=str,
)
@click.pass_context
def do_purge_user(
    ctx,
    username: str,
):
    """
    Purge the given username (interactively).
    """
    config = ctx.obj
    token = (
        config.token
        or xdg_token()
        or die("One of --token-file or --token-env is required")
    )
    instance = config.instance
    builder = (
        ds.Builder.create_from_url(instance, token)
        .set_readonly(config.readonly)
        .set_username(username)
    )

    user = builder.build_user()
    ts = datetime.datetime.fromisoformat(user.user.created_at)
    ts = ts.strftime("%Y-%m-%d")
    click.echo(
        f"User: {user.user.username} ({user.user.name}), created {ts}, profile {user.user.web_url}"
    )
    for issue in user.issues:
        click.echo(f"{'':3s}  {'':25s}  {issue.web_url}: {issue.title}")
    if not user.issues:
        click.secho("- user has no issues filed", fg="red")
    click.secho(
        "Purging a user means a full delete including all issues, MRs, etc. This is nonrecoverable!",
        bold=True,
    )
    confirmed = click.confirm(f"Purging user {username}", default=False)
    if confirmed:
        user.delete()


def request_webhook(
    params: Params, project: str, template_type: Type[ds.RequestWebhookTemplate]
):
    if project.startswith("https://"):
        if not project.startswith(params.instance):
            click.echo("Invalid URL, --instance arg required")
            raise SystemExit(1)

        proj = Project.from_url(project)
    else:
        proj = Project.from_spec(project)

    if not proj:
        click.echo(f"Failed to parse project {project}")
        raise SystemExit(1)

    builder = (
        ds.Builder.create_from_url(params.instance, params.token)
        .set_project(proj.path_with_namespace)
        .set_readonly(params.config.readonly)
    )

    dam = builder.build_project()
    if not dam.user_is_maintainer():
        click.echo("This request can only be filed by a Maintainer (or above)")
        raise SystemExit(1)

    assert dam.user
    template = template_type.from_gitlab(dam.project, dam.user)
    issue = dam.file_webhook_request_issue(template)
    click.echo(f"Issue filed as: {issue}")


@damspam.command(name="request-webhook", help="Add a webhook to a project")
@click.argument(
    "project",
    type=str,
)
@click.pass_context
def do_request_webhook(
    ctx,
    project: str,
):
    """
    Requests a webhook for the given project, given in the form namespace/project or simply as
    full URL, e.g. https://gitlab.freedesktop.org/foo/bar/

    A webhook must be requested by a Maintainer (or above) of the given project. This is a two-step
    process, this command merely files an issue in a standardized format against
    freedesktop/fdo-bots requesting the webhook. The actual processing is done elsewhere.

    Note that this command falls back to XDG_CONFIG_HOME/damspam/user.token if neither
    --token-env or --token-file is given.
    """
    config = ctx.obj
    token = (
        config.token
        or xdg_token("user.token")
        or die("One of --token-file or --token-env is required")
    )
    instance = config.instance
    params = Params(config=config, instance=instance, token=token)  # type: ignore
    request_webhook(params, project, template_type=ds.DamspamRequestWebhookTemplate)


def process_webhook_request(
    params: Params,
    issue: str,
    bot: ds.Bot,
):
    iss = Issue.from_url(issue)
    if not iss:
        die(f"Failed to parse issue {issue}")

    project = iss.project.path_with_namespace
    issue_iid = iss.iid
    builder = (
        ds.Builder.create_from_url(params.instance, params.token)
        .set_project(project)
        .set_issue_iid(issue_iid)
        .set_readonly(params.config.readonly)
    )

    ds_issue = builder.build_issue()
    if ds_issue.is_closed:
        die("Issue already closed")
    if ds_issue.is_assigned_to(bot.id):
        die(f"Issue already assigned to @{bot.username}")
    error = ds_issue.process_webhook_request(bot)
    if error is None:
        message = "Request processed successful"
    else:
        message = error
    ds_issue.update_with_message(
        message,
        close=error is None,
        is_success=error is None,
        label_prefix=bot.label,
        assign_to=bot.id,
    )


@damspam.command(
    name="process-webhook-request",
    help="Process a webhook request",
)
@click.argument(
    "issue",
    type=str,
    required=True,
)
@click.pass_context
def do_process_webhook_request(
    ctx,
    issue: str,
):
    """
    Process a request for a webhook (see the request-webhook command)
    and handles that issue accordingly.
    """
    config = ctx.obj
    instance = config.instance
    token = (
        config.token
        or xdg_token()
        or die("One of --token-file or --token-env is required")
    )
    params = Params(config=config, instance=instance, token=token)  # type: ignore
    process_webhook_request(params, issue, ds.SPAMBOT)


@damspam.command(
    name="sanitize-profile",
    help="Sanitize a user profile",
)
@click.option(
    "--delay",
    type=int,
    default=0,
    help="Timeout in seconds before doing any actual work",
)
@click.option(
    "--internal-users",
    is_flag=True,
    help="Apply to internal users, not just external ones",
)
@click.option(
    "--force",
    is_flag=True,
    help="Always sanitize the bio, even if expected fields are empty",
)
@click.option(
    "--use-user-id",
    is_flag=True,
    default=False,
    help="Interpret the username as id (True if using a JSON payload)",
)
@click.argument(
    "username",
    type=str,
    required=True,
)
@click.pass_context
def do_sanitize_profile(
    ctx,
    delay: int,
    username: Optional[str],
    internal_users: bool,
    force: bool,
    use_user_id: bool,
):
    """
    Sanitize a user's bio/weburl/twitter links etc (resetting all to empty values).
    """
    config = ctx.obj
    instance = config.instance

    user_id = -1

    if use_user_id:
        try:
            user_id = int(username)
        except ValueError as e:
            die(f"Invalid user ID: {e}")

    token = (
        config.token
        or xdg_token()
        or die("One of --token-file or --token-env is required")
    )

    if delay > 0:
        time.sleep(delay)

    try:
        builder = ds.Builder.create_from_url(instance, token).set_readonly(
            config.readonly
        )
        if use_user_id:
            assert user_id != -1
            builder.set_user_id(user_id)
        else:
            assert username is not None
            builder.set_username(username)
        ds_user = builder.build_user()
        ds_user.sanitize(only_external=(not internal_users), force=force)
    except ds.BuilderError as e:
        logger.critical(e)


@damspam.command(
    name="sanitize-profiles",
    help="Sanitize multiple user profiles",
)
@click.option(
    "--created-after",
    type=str,
    help="The time-frame to check (default: 24h ago)",
    default="24h ago",
)
@click.pass_context
def do_sanitize_profiles(
    ctx,
    created_after: str,
):
    """
    This command sanitizes the all users bio/weburl/twitter links etc (resetting all to empty values) within
    the given relative interval (e.g. 2h ago').
    """
    import dateparser

    config = ctx.obj
    token = (
        config.token
        or xdg_token()
        or die("One of --token-file or --token-env is required")
    )
    instance = config.instance
    builder = ds.Builder.create_from_url(instance, token).set_readonly(config.readonly)

    now = datetime.datetime.now(tz=datetime.timezone.utc)
    then = dateparser.parse(
        created_after,
        settings={
            "RELATIVE_BASE": now,
            "TIMEZONE": "UTC",
            "RETURN_AS_TIMEZONE_AWARE": True,
        },
    )

    dam = builder.build_user_list()
    q = queue.Queue()
    dam.list_profile_spammers(cutoff=then, queue=q, include_sanitized_users=False)
    while True:
        spammer = q.get()
        # If the queue returns None, we're done
        if spammer is None:
            break

        spammer.sanitize()
        q.task_done()

    q.task_done()


@damspam.command(
    name="purge-profile-spammers", help="Interactively delete profile spammers"
)
@click.option(
    "--since",
    type=str,
    help="The time-frame to check (default: 48h ago)",
    default="48h ago",
)
@click.option(
    "--include-sanitized-profiles",
    is_flag=True,
    help="Include users with profiles already sanitized by damspam",
)
@click.pass_context
def do_purge_profile_spammers(
    ctx,
    since: str,
    include_sanitized_profiles: bool,
):
    """
    List all users with a nonempty profile and allow for deletion of those users.
    """
    import dateparser

    config = ctx.obj
    token = (
        config.token
        or xdg_token()
        or die("One of --token-file or --token-env is required")
    )
    instance = config.instance
    builder = ds.Builder.create_from_url(instance, token).set_readonly(config.readonly)

    now = datetime.datetime.now(tz=datetime.timezone.utc)
    then = dateparser.parse(
        since,
        settings={
            "RELATIVE_BASE": now,
            "TIMEZONE": "UTC",
            "RETURN_AS_TIMEZONE_AWARE": True,
        },
    )

    dam = builder.build_user_list()
    q = queue.Queue()

    def find_spammers():
        dam.list_profile_spammers(
            cutoff=then, queue=q, include_sanitized_users=include_sanitized_profiles
        )

    thread = threading.Thread(target=find_spammers, daemon=True)
    thread.start()

    click.secho(
        "Purging a user means a full delete including all issues, MRs, etc. This is nonrecoverable!",
        bold=True,
        fg="red",
    )

    while True:
        spammer = q.get()
        # If the queue returns None, we're done
        if spammer is None:
            q.task_done()
            thread.join()
            break
        click.secho("-" * 60, fg="blue")
        user = spammer.user
        click.secho(f"{user.username:20s} | {user.name}", bold=True)
        ts = datetime.datetime.fromisoformat(user.created_at)
        ts = ts.strftime("%Y-%m-%d")
        click.echo(f"Created:  {ts}")
        if user.location:
            click.echo(f"Location: {user.location}")
        if user.website_url:
            click.echo(f"Web:      {user.website_url}")
        social = user.twitter or user.linkedin or user.discord
        if social:
            click.echo(f"Social:   {social}")
        if user.bio:
            click.echo(f"Bio:      {user.bio}")
        if user.note:
            click.echo(f"Note:      {user.note}")
        value: str = click.prompt("Purge? [y/N]", type=str)
        if value == "y":
            spammer.delete()
        q.task_done()

    click.echo("All spammers processed")


@click.group()
@click.option("--verbose", "-v", is_flag=True, help="Enable debug logging")
@click.option("--readonly", is_flag=True, help="Print, but don't modify")
@click.option(
    "--token-file",
    type=click.Path(exists=True, dir_okay=False),
    help="File containing the GitLab API token. Defaults to XDG_CONFIG_HOME/bugbot/bugbot.token",
)
@click.option(
    "--token-env",
    type=str,
    help="Environment variable containing the GitLab API token",
)
@click.option(
    "--instance",
    type=str,
    help=f"The GitLab instance (default: {DEFAULT_INSTANCE}",
)
@click.pass_context
def bugbot(
    ctx,
    verbose: bool,
    readonly: bool,
    token_file: Optional[click.Path],
    token_env: Optional[str],
    instance: Optional[str],
):
    global logger
    logger.setLevel(logging.DEBUG if verbose else logging.INFO)

    if token_file is not None:
        token = open(token_file).read().rstrip() or die("Empty token file")  # type: ignore
    elif token_env is not None:
        token = os.environ.get(token_env) or die(
            "Requested token environment variable does not exist, use --token-env"
        )
    else:
        token = None

    # Note: this not a click default so we can patch it in the tests
    instance = instance or DEFAULT_INSTANCE
    ctx.obj = Config(instance=instance, verbose=verbose, readonly=readonly, token=token)


@bugbot.command(name="request-webhook", help="Add a webhook to a project")
@click.argument(
    "project",
    type=str,
)
@click.pass_context
def bugbot_request_webhook(
    ctx,
    project: str,
):
    """
    Requests a webhook for the given project, given in the form namespace/project or simply as
    full URL, e.g. https://gitlab.freedesktop.org/foo/bar/

    A webhook must be requested by a Maintainer (or above) of the given project. This is a two-step
    process, this command merely files an issue in a standardized format against
    freedesktop/fdo-bots requesting the webhook. The actual processing is done elsewhere.

    Note that this command falls back to XDG_CONFIG_HOME/bugbot/user.token if neither
    --token-env or --token-file is given.
    """

    config = ctx.obj
    token = (
        config.token
        or xdg_token("user.token", project="bugbot")
        or die("One of --token-file or --token-env is required")
    )
    instance = config.instance
    params = Params(config=ctx.obj, instance=instance, token=token)  # type: ignore
    request_webhook(params, project, template_type=ds.BugbotRequestWebhookTemplate)


@bugbot.command(
    name="process-webhook-request",
    help="Process a webhook request",
)
@click.argument(
    "issue",
    type=str,
    required=True,
)
@click.pass_context
def bugbot_process_webhook_request(
    ctx,
    issue: str,
):
    """
    Process a request for a webhook (see the request-webhook command)
    and handles that issue accordingly.
    """
    config = ctx.obj
    instance = config.instance
    token = (
        config.token
        or xdg_token()
        or die("One of --token-file or --token-env is required")
    )
    params = Params(config=config, instance=instance, token=token)  # type: ignore
    process_webhook_request(params, issue, ds.BUGBOT)


@bugbot.command(
    name="gitlab-triage",
    help="Run gitlab-triage against a project",
)
@click.option(
    "--branches",
    multiple=True,
    type=str,
    default=["main", "master"],
    help="The branch name to fetch the .triage-policies.yml file from",
)
@click.option(
    "--delay", type=int, default=0, help="The delay before invoking gitlab-triage"
)
@click.option(
    "--timeout",
    type=int,
    default=60,
    help="The number of seconds to wait for gitlab-triage to complete",
)
@click.argument(
    "issue",
    type=str,
    required=True,
)
@click.pass_context
def bugbot_gitlab_triage(
    ctx,
    branches: Tuple[str],
    issue: str,
    timeout: int,
    delay: int,
):
    """
    Fetch the .triage-policies.yml file from the given project and run
    gitlab-triage against it for the given issue or merge request URL.

    If a nonzero delay is provided (and after fetching the policies file) wait
    for the given number of seconds before executing gitlab-triage. This delay
    should allow a user to remove accidentally assigned labels before gitlab-triage
    modifies the issue or merge request.
    """
    config = ctx.obj
    instance = config.instance
    token = (
        config.token
        or xdg_token("user.token", project="bugbot")
        or die("One of --token-file or --token-env is required")
    )
    iss = Issue.from_url(issue)
    mr = MergeRequest.from_url(issue)
    if not iss and not mr:
        die(f"Failed to parse URL {issue}")

    source = iss or mr
    assert source is not None

    POLICYFILE = ".triage-policies.yml"

    for branch in branches:
        url = f"{instance.rstrip('/')}/{source.project.path_with_namespace}/-/raw/{branch}/{POLICYFILE}"
        r = requests.get(url)
        if r.status_code != 404:
            logger.debug(f"Found triage policies on branch {branch}")
            break
    else:
        die(f"Unable to find {POLICYFILE}, branches tried: {', '.join(branches)}")

    if delay > 0:
        time.sleep(delay)

    with tempfile.TemporaryDirectory(ignore_cleanup_errors=True) as tmpdir:
        policyfile = Path(tmpdir) / "triage-policies.yml"
        with open(policyfile, "w") as fd:
            fd.write(r.text)

        args = [
            "-f",
            str(policyfile),
            "--host-url",
            instance,
            "--source-id",
            source.project.path_with_namespace,
            "--resource-reference",
            f"{source.separator}{source.iid}",
        ]
        if config.readonly:
            args += ["--dry-run"]
        logger.debug(f"running gitlab-triage --token XXX {' '.join(args)}")
        # we don't want to leak the token, so let's add after the debug message
        args = ["--token", token] + args
        p = subprocess.Popen(
            ["gitlab-triage"] + args,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            encoding="utf-8",
            cwd=tmpdir,
        )
        try:
            stdout, stderr = p.communicate(timeout=timeout)
        except subprocess.TimeoutExpired:
            logger.error("Process did not finish, killing it")
            p.kill()
            stdout, stderr = p.communicate()

        logger.warning(stderr)
        click.echo(stdout)

        if p.returncode:
            raise SystemExit(p.returncode)


if __name__ == "__main__":
    damspam()
